﻿using Sol_EF_Spliting.EF;
using Sol_EF_Spliting.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Spliting
{
    public class UserDal
    {
        private UserDBEntities dbObj = null;

        public UserDal()
        {
            dbObj = new UserDBEntities();
        }

        #region public method

        //public async Task<IEnumerable<UserEntity>> Search(UserEntity userEntityObj)
        //{
        //    ObjectParameter status = null;
        //    ObjectParameter message = null;
        //    try
        //    {
        //        return await Task.Run(() => {

        //            var getQuery =
        //                dbObj
        //                .tblUsers
        //                    ?.AsEnumerable()
        //                    ?.Select(this.GetSelect)
        //                    ?.ToList();

        //            return getQuery;
        //        });
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public async Task<IEnumerable<UserEntity>> GetUserData(UserEntity userEntityObj)
        {
            ObjectParameter status = null;
            ObjectParameter message = null;
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        dbObj
                        .tblUsers
                            ?.AsEnumerable()
                            ?.Select(this.GetSelect)
                            ?.ToList();

                    return getQuery;
                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property

        private Func<tblUser, UserEntity> GetSelect
        {
            get
            {
                return
                    (letblUserObj) => new UserEntity()
                    {
                        UserId = letblUserObj.UserId,
                        FirstName = letblUserObj.FirstName,
                        LastName = letblUserObj.LastName,
                        
                        UserLogin = new UserLoginEntity()
                        {
                            UserName = letblUserObj.UserName,
                            Password = letblUserObj.Password,
                        },
                        UserCommunication = new UserCommunication()
                        {
                            EmailId = letblUserObj.EmailId,
                            MobileNo = letblUserObj.MobileNo
                        }
                    };
            }
        }
        #endregion
    }
}
