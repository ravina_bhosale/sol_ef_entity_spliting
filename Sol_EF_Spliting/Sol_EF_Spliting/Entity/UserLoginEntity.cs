﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Spliting.Entity
{
    public class UserLoginEntity
    {
        public decimal UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}
